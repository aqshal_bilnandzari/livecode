Rails.application.routes.draw do
  # get 'students/index'
  resources :students
  resources :teachers
  resources :exams

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
